Class {
	#name : #CrossMorph,
	#superclass : #Morph,
	#category : #'PBE-Morphic'
}

{ #category : #testing }
CrossMorph >> containsPoint: aPoint [
	^ (self horizontalBar containsPoint: aPoint) or: [ self verticalBar containsPoint: aPoint ]
]

{ #category : #drawing }
CrossMorph >> drawOn: aCanvas [
	aCanvas fillRectangle: self horizontalBar color: self color.
	aCanvas fillRectangle: self verticalBar color: self color.
]

{ #category : #'event handling' }
CrossMorph >> handlesKeyDown: anEvent [
	^ true
]

{ #category : #'event handling' }
CrossMorph >> handlesMouseDown: anEvent [
	^ true

]

{ #category : #'event handling' }
CrossMorph >> handlesMouseOver: anEvent [
	^ true
]

{ #category : #drawing }
CrossMorph >> horizontalBar [
	| crossHeight |
	crossHeight := (self height / 3) rounded.
	^ self bounds insetBy: 0 @ crossHeight

]

{ #category : #initialization }
CrossMorph >> initialize [
	super initialize.
	self startStepping
]

{ #category : #'event handling' }
CrossMorph >> keyDown: anEvent [
	| key |
	key := anEvent key.
	key = KeyboardKey up ifTrue: [ self position: self position - (0 @ 10) ].
	key = KeyboardKey down ifTrue: [ self position: self position + (0 @ 10) ].
	key = KeyboardKey right ifTrue: [ self position: self position + (10 @ 0) ].
	key = KeyboardKey left ifTrue: [ self position: self position - (10 @ 0) ].
]

{ #category : #'event handling' }
CrossMorph >> mouseDown: anEvent [
	anEvent redButtonPressed
		ifTrue: [ self color: Color red ]. "click"
	anEvent yellowButtonPressed
		ifTrue: [ self color: Color yellow ]. "action-click"
	self changed

]

{ #category : #'event handling' }
CrossMorph >> mouseEnter: anEvent [
	anEvent hand newKeyboardFocus: self
]

{ #category : #'event handling' }
CrossMorph >> mouseLeave: anEvent [
	anEvent hand releaseKeyboardFocus: self
]

{ #category : #stepping }
CrossMorph >> step [
	(self color diff: Color black) < 0.1
		ifTrue: [ self color: Color red ]
		ifFalse: [ self color: self color darker ]

]

{ #category : #stepping }
CrossMorph >> stepTime [
	^ 100
]

{ #category : #drawing }
CrossMorph >> verticalBar [
	| crossWidth |
	crossWidth := (self width / 3) rounded.
	^ self bounds insetBy: crossWidth @ 0
]
