Class {
	#name : #ReceiverMorph,
	#superclass : #Morph,
	#category : #'PBE-Morphic'
}

{ #category : #initialization }
ReceiverMorph >> initialize [
	super initialize.
	color := Color red.
	bounds := 0 @ 0 extent: 200 @ 200

]

{ #category : #'dropping/grabbing' }
ReceiverMorph >> repelsMorph: aMorph event: anEvent [
	^ (self wantsDroppedMorph: aMorph event: anEvent) not

]

{ #category : #'drag and drop' }
ReceiverMorph >> wantsDroppedMorph: aMorph event: anEvent [
	^ aMorph color = Color blue
]
