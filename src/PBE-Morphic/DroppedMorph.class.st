Class {
	#name : #DroppedMorph,
	#superclass : #Morph,
	#category : #'PBE-Morphic'
}

{ #category : #initialization }
DroppedMorph >> initialize [
	super initialize.
	color := Color blue.
	self position: 250 @ 100
]

{ #category : #'dropping/grabbing' }
DroppedMorph >> rejectDropMorphEvent: anEvent [
	| h |
	h := anEvent hand.
	anEvent wasHandled: true
]
